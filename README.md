# Overview

This repostiory is used to deploy the Price a Tray game on a ConfigSync enabled GDCE, GDCV or GKE cluster

> :warning: NOTE -- Part of the configuration is defined in the Primary Root Repository. The pattern using "variants" is being developed and will be updated here upon completion. Currently this Application Package is missing configuration related to `ConfigMap` objects that vary between clusters. Stay tuned!

## Fleet Resource Bundle

To generate the fleet resource bundle, set up `.envrc` or ENV vars then run the `envsubst` command to replace the variables

### Variables

| Variable             | Definition                              | Example                                      |
|----------------------|-----------------------------------------|----------------------------------------------|
| `${PROJECT_ID}`      | GCP Project                             | anthos-bare-metal-project                    |
| `${GSA_NAME}`        | GCP GSA for CloudBuild repo v2 access   | fp-gcb-gsa@<project>.iam.gserviceaccount.com |
| `${PACKAGE_NAME}`    | Application name                        | price-a-tray-package                         |
| `${PACKAGE_VERSION}` | Application version (git-tag)           | v0.0.1                                       |
| `${CONNECTION_NAME}` | Defined "Connection" name in CloudBuild | price-a-tray                                 |
| `${REPOSITORY_NAME}` | CloudBuild repository name              | price-a-tray-game-package                    |

```shell
envsubst < fleet-resource-bundle.yaml.template > fleet-resource-bundle.yaml
```

### Fleet Package Variant Variables
| Variable | Description | Pattern | File Name |
|----------|-------------|---------|-----------|
| `membership.name` | The name of the Fleet membership (often the cluster name) | app-config-${membership.name} | `app-config-gdc-pae1.yaml` |
| `membership.location` | The location of the cluster in the fleet membership | app-config-${membership.location} | `app-config-global.yaml` |
| `membership.labels[‘store’]` | The value of a cluster label | app-config-$membership.labels[‘store’] | Labe is `store=corporate` => `app-config-corporate.yaml` |

## How to use

Must apply the following code snippet. This code will create a new `RepoSync` object pointing to the Cluster Trait Repo, and create an `ExternalSecret` that allows the `RootSync` to reference a private repository.

> :note: This can be applied via ACM updates IF the Primary Root Repo is `unstructured`, otherwise a manual `kubectl apply -f <file.yaml>` is required to install this CTR.

```yaml
apiVersion: configsync.gke.io/v1beta1
kind: RepoSync
metadata:
  name: pat-pkg                                      # 7 characters
  namespace: price-a-tray
  annotations:
    configsync.gke.io/deletion-propagation-policy: Foreground
spec:
  sourceFormat: "unstructured"
  git:
    repo: "https://gitlab.com/mike-ensor/price-a-tray-game-package.git"
    branch: "main"
    dir: "/config/<CLUSTER_NAME>"                    # NOTE: Name of the cluster is required (and must be a varient)
    auth: "token"
    secretRef:
      name: price-a-tray-pkg-git-creds               # matches the ExternalSecret spec.target.name below

---

kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: pat-pkg-ksa-repo
  namespace: price-a-tray
subjects:
  - kind: ServiceAccount
    name: ns-reconciler-price-a-tray-pat-pkg-7      # k get sa -n config-management-system  ( ns-reconciler-{NAMESPACE}-{REPO_SYNC_NAME}-{REPO_SYNC_NAME_LENGTH} )
    namespace: config-management-system
roleRef:
  kind: Role
  name: reconciler-admin                            # Granular control over resources can be created with RBAC (Future demo will explore custom RBAC)
  apiGroup: rbac.authorization.k8s.io

---

# Role for Reconciler
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: point-of-sales
  name: reconciler-admin
rules:
- apiGroups: ["*"]
  resources: ["*"]
  verbs: ["*"]

---

apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: price-a-tray-pkg-git-creds-es
  namespace: config-management-system
spec:
  refreshInterval: 24h
  secretStoreRef:
    kind: ClusterSecretStore
    name: gcp-secret-store
  target:                                       # K8s secret definition
    name: price-a-tray-pkg-git-creds            ############# Matches the secretRef in the RepoSync object's authentication
    creationPolicy: Owner
  data:
  - secretKey: username                         # K8s secret key name inside secret
    remoteRef:
      key: price-a-tray-pkg-access-token-creds  #  GCP Secret Name
      property: username                        # field inside GCP Secret
  - secretKey: token                            # K8s secret key name inside secret
    remoteRef:
      key: price-a-tray-pkg-access-token-creds  #  GCP Secret Name
      property: token                           # field inside GCP Secret

```

### Create GCP Secret for git-creds

Create the GCP Secret Manager secret used by `ExternalSecret` to proxy for K8s `Secret`

```
export PROJECT_ID=<your google project id>
export SCM_TOKEN_TOKEN=<your gitlab personal-access token value>
export SCM TOKEN_USER=<your gitlab personal-access token user>

gcloud secrets create price-a-tray-pkg-access-token-creds --replication-policy="automatic" --project="${PROJECT_ID}"
echo -n "{\"token\"{{':'}} \"${SCM_TOKEN_TOKEN}\", \"username\"{{':'}} \"${SCM_TOKEN_USER}\"}" | gcloud secrets versions add price-a-tray-pkg-access-token-creds --project="${PROJECT_ID}" --data-file=-

```

## Local Validation

Assuming `nomos` is installed (via `gcloud components install nomos`)

```
nomos vet --no-api-server-check --path config/
```

### Docker method

Using this link to find the version of nomos-docker:  https://cloud.google.com/anthos-config-management/docs/how-to/updating-private-registry#expandable-1

```
docker pull gcr.io/config-management-release/nomos:stable
docker run -it -v $(pwd):/code/ gcr.io/config-management-release/nomos:stable nomos vet --no-api-server-check --path /code/config/
```

### ConfigSync Overview

See [our documentation](https://cloud.google.com/anthos-config-management/docs/repo) for how to use each subdirectory.

## Series Links

This project is one component in a multi-component solution. The composed parts make up the game "Price a Tray".

https://gitlab.com/mike-ensor/price-a-tray-dynamic-frontend
: This is the User Experience and User Interface for the game

https://gitlab.com/mike-ensor/price-a-tray-backend
: This is the Game API where rules, game lifecycle, scores and data are stored

https://gitlab.com/mike-ensor/rtsp-to-mjpeg.git
: This project turns a RTSP stream into a MJPEG stream for consumption by the UI

https://gitlab.com/mike-ensor/price-a-tray-game-package
: This is the project used to deploy to a Kubernetes project with ConfigSync installed
